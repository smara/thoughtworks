//
//  Artist.swift
//  ShuffleSongs
//
//  Created by Silvia Florido on 02/02/19.
//  Copyright © 2019 Silvia Florido. All rights reserved.
//

import Foundation

class Artist: Equatable, NSCopying {
    
    let artistId: Int
    let artistName: String
    let primaryGenreName: String?
    
    var songs = [Song]()
    
    init?(fromTWClient dict: [String:AnyObject]) {
        guard let id = dict[TWClient.JSONResponseKeys.ArtistId] as? Int, let artistName = dict[TWClient.JSONResponseKeys.ArtistName] as? String else {
            return nil
        }
        artistId = id
        self.artistName = artistName
        self.primaryGenreName = dict[TWClient.JSONResponseKeys.ArtistPrimaryGenreName] as? String
    }
    
    init(artistId:Int, artistName:String, primaryGenreName:String?) {
        self.artistId = artistId
        self.artistName = artistName
        self.primaryGenreName = primaryGenreName
    }
    
    
    //MARK:- Equatable
    static func ==(lhs: Artist, rhs: Artist) -> Bool {
        return lhs.artistId == rhs.artistId
    }
    
    //MARK:- NSCopying
    func copy(with zone: NSZone? = nil) -> Any {
        let artist = Artist(artistId: self.artistId, artistName: self.artistName, primaryGenreName: self.primaryGenreName)
        for song in songs {
            let song = Song(songId: song.songId, trackName: song.trackName, artworkUrl: song.artworkUrl, artistId: song.artistId, artistName: song.artistName, primaryGenreName: song.primaryGenreName)
            artist.songs.append(song)
        }
        return artist
    }
    
}
