//
//  Song.swift
//  ShuffleSongs
//
//  Created by Silvia Florido on 02/02/19.
//  Copyright © 2019 Silvia Florido. All rights reserved.
//

import Foundation


class Song: Equatable, NSCopying {
    
    let songId: Int
    let trackName: String
    let artworkUrl: String?
    let artistId: Int?
    let artistName: String?
    let primaryGenreName: String?
    
    init?(fromTWClient dict: [String:AnyObject]) {
        guard let id = dict[TWClient.JSONResponseKeys.SongId] as? Int, let trackName = dict[TWClient.JSONResponseKeys.TrackName] as? String else {
            return nil
        }
        songId = id
        self.trackName = trackName
        artworkUrl = dict[TWClient.JSONResponseKeys.ArtworkUrl] as? String
        artistId = dict[TWClient.JSONResponseKeys.SongArtistId] as? Int
        artistName = dict[TWClient.JSONResponseKeys.SongArtistName] as? String
        primaryGenreName = dict[TWClient.JSONResponseKeys.SongPrimaryGenreName] as? String
    }
    
    init(songId:Int, trackName:String, artworkUrl:String?, artistId:Int?, artistName:String?, primaryGenreName:String? ) {
        self.songId = songId
        self.trackName = trackName
        self.artworkUrl = artworkUrl
        self.artistId = artistId
        self.artistName = artistName
        self.primaryGenreName = primaryGenreName
    }
    
    //MARK:- Equatable
    static func ==(lhs: Song, rhs: Song) -> Bool {
        return lhs.songId == rhs.songId
    }
    
    
    //MARK:- NSCopying
    func copy(with zone: NSZone? = nil) -> Any {
        let song = Song(songId:self.songId, trackName: self.trackName, artworkUrl: self.artworkUrl, artistId: self.artistId, artistName: self.artistName, primaryGenreName:self.primaryGenreName)
        return song
    }
}
