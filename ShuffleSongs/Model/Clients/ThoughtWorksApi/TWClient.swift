//
//  TWClient.swift
//  ShuffleSongs
//
//  Created by Silvia Florido on 02/02/19.
//  Copyright © 2019 Silvia Florido. All rights reserved.
//

import Foundation
import UIKit

class TWClient {
    var session = URLSession.shared
    let imageCache = NSCache<NSString, UIImage>()
    
    class func sharedInstance() -> TWClient {
        struct Singleton {
            static var sharedInstance = TWClient()
        }
        return Singleton.sharedInstance
    }
    
    
    // MARK:- Specific Requests
    func getAllSongsList(_ completionHandler: @escaping (_ results: [Song]?, _ error: NSError?) -> Void) {
        let parameters: [String:AnyObject] = [TWClient.ParameterKeys.Id:"909253,1171421960,358714030,1419227,264111789" as AnyObject,
                          TWClient.ParameterKeys.Limit:"10" as AnyObject]
        let method = TWClient.Methods.Lookup
        
        taskForGET(method, parameters: parameters) { (results, error) in
            guard (error == nil) else {
                completionHandler(nil, error)
                return
            }
            self.convertResponseToSongs(results, completionHandlerForSongs: completionHandler)
        }
    }
    
    private func convertResponseToSongs(_ results: AnyObject?, completionHandlerForSongs: (_ result: [Song]?, _ error: NSError?) -> Void) {
        guard let results = results?[TWClient.JSONResponseKeys.Results] as? [[String:AnyObject]] else {
            let error = NSError(domain: "Error when trying to convert response data to Songs", code: 1, userInfo: nil)
            completionHandlerForSongs(nil, error)
            return
        }
        print(results)
        var songs = [Song]()
        
        for result in results {
            guard let wrapperType = result[TWClient.JSONResponseKeys.WrapperType] as? String else {
                continue
            }
            
            if wrapperType == "track" {
                if let newSong = Song(fromTWClient: result) {
                    songs.append(newSong)
                }
            }
        }
        completionHandlerForSongs(songs, nil)
    }
    

    
    func getArtistsWithSongs(_ completionHandler: @escaping (_ artists: [Artist]?, _ error: NSError?) -> Void) {
        let parameters: [String:AnyObject] = [TWClient.ParameterKeys.Id:"909253,1171421960,358714030,1419227,264111789" as AnyObject,
                                              TWClient.ParameterKeys.Limit:"10" as AnyObject]
        let method = TWClient.Methods.Lookup
        
        taskForGET(method, parameters: parameters) { (results, error) in
            guard (error == nil) else {
                completionHandler(nil, error)
                return
            }
            self.buildModel(results, completionHandler: completionHandler)
        }
    }
    
    
    private func buildModel(_ results: AnyObject?, completionHandler: (_ artists: [Artist]?, _ error: NSError?) -> Void) {
        guard let results = results?[TWClient.JSONResponseKeys.Results] as? [[String:AnyObject]] else {
            let error = NSError(domain: "Error when trying to convert response data to Songs", code: 1, userInfo: nil)
            completionHandler(nil, error)
            return
        }
        
        var artists = [Artist]()
        var songs = [Song]()
        
        // split by data type, song or artist
        for result in results {
            guard let wrapperType = result[TWClient.JSONResponseKeys.WrapperType] as? String else {
                continue
            }
            
            if wrapperType == "artist" {
                if let newArtist = Artist(fromTWClient: result) {
                    if (artists.filter{ $0.artistId == newArtist.artistId}.count == 0) {
                        artists.append(newArtist)
                    }
                }
            } else if wrapperType == "track" {
                if let newSong = Song(fromTWClient: result) {
                    if (songs.filter{ $0.songId == newSong.songId }.count == 0) {
                        songs.append(newSong)
                    }
                }
            }
        }
        
        // associate songs to artists
        for song in songs {
            guard let artistIdInSong = song.artistId else {
                continue
            }
            
            guard let artistFound: Artist = (artists.filter{ $0.artistId == artistIdInSong}).first else {
                continue
            }
            artistFound.songs.append(song)
            if let idx = songs.index(of: song) {
                songs.remove(at: idx)
            }
            
            // add the songs kept in the array to unknown artist
        }
        completionHandler(artists, nil)
    }
    
    
    func getImageFor(_ urlString: String, completionHandlerForImage: @escaping (_ image: UIImage?, _ error: NSError?) -> Void) -> Void {
        
        if let cachedImage = imageCache.object(forKey: urlString as NSString) {
            completionHandlerForImage(cachedImage, nil)
        } else {
            TWClient.sharedInstance().taskForGETImage(urlString, completionHandlerForImage: { (imageData, error) in
                if error != nil  {
                    completionHandlerForImage(nil, error)
                } else {
                    if let imageData = imageData, let image = UIImage(data: imageData) {
                        self.imageCache.setObject(image, forKey: urlString as NSString)
                        completionHandlerForImage(image, nil)
                    }
                }
            })
        }
        
    }
    
    
    
    
    // MARK:- Base Requests
    private func taskForGET(_ method: String, parameters: [String:AnyObject], completionHandler: @escaping (_ result: AnyObject?, _ error: NSError?) -> Void) {
        func sendError(_ error: String) {
            print(error)
            let userInfo = [NSLocalizedDescriptionKey : error]
            completionHandler(nil, NSError(domain: "taskForGETMethod", code: 1, userInfo: userInfo))
        }
        
        guard let url = urlFromParameters(parameters, withMethod: method) else {
            sendError("Error to create URL")
            return
        }
        print(url)
        
        let request = URLRequest(url: url)
        
        let task = session.dataTask(with: request) { (data, response, error) in
            guard (error == nil) else {
                sendError("There was an error with your request: \(error!)")
                return
            }

            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                sendError("Your request returned a status code other than 2xx!")
                return
            }
            
            guard let data = data else {
                sendError("No data was returned by the request!")
                return
            }
            
            self.convertDataWithCompletionHandler(data, completionHandlerForConvertData: completionHandler)
        }
        task.resume()
    }
    
    
    

    private func taskForGETImage(_ urlString: String, completionHandlerForImage: @escaping (_ imageData: Data?, _ error: NSError?) -> Void) -> Void {
        
        func sendError(_ error: String) {
            print(error)
            let userInfo = [NSLocalizedDescriptionKey : error]
            completionHandlerForImage(nil, NSError(domain: "taskForGETMethod", code: 1, userInfo: userInfo))
        }
        
        guard let url = URL(string: urlString) else {
            sendError("Error to create URL")
            return
        }
        print("Request to get image sent.")
        let request = URLRequest(url: url)
        let task = session.dataTask(with: request) { (data, response, error) in
            guard (error == nil) else {
                sendError("There was an error with your request: \(error!)")
                return
            }
            
            guard let statusCode = (response as? HTTPURLResponse)?.statusCode, statusCode >= 200 && statusCode <= 299 else {
                sendError("Your request returned a status code other than 2xx!")
                return
            }
            
            guard let data = data else {
                sendError("No data was returned by the request!")
                return
            }
            
            completionHandlerForImage(data, nil)
        }
        task.resume()
        
    }

    
    
    
    
    
    // MARK:- Helpers
    private func convertDataWithCompletionHandler(_ data: Data, completionHandlerForConvertData: (_ result: AnyObject?, _ error: NSError?) -> Void) {
        
        var parsedResult: AnyObject? = nil
        do {
            parsedResult = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as AnyObject
            print(parsedResult as Any)
        } catch {
            let userInfo = [NSLocalizedDescriptionKey : "Could not parse the data as JSON: '\(data)'"]
            completionHandlerForConvertData(nil, NSError(domain: "convertDataWithCompletionHandler", code: 1, userInfo: userInfo))
        }
        
        completionHandlerForConvertData(parsedResult, nil)
    }
    
    
    
    private func urlFromParameters(_ parameters: [String:AnyObject], withMethod: String? = nil) -> URL? {
        var components = URLComponents()
        components.scheme = TWClient.Api.Scheme
        components.host = TWClient.Api.Host
        components.path = withMethod ?? ""
        components.queryItems = [URLQueryItem]()
        
        for (key,value) in parameters {
            let queryItem = URLQueryItem(name: key, value: "\(value)")
            components.queryItems!.append(queryItem)
        }
        return components.url
    }
}










