//
//  TWConstants.swift
//  ShuffleSongs
//
//  Created by Silvia Florido on 02/02/19.
//  Copyright © 2019 Silvia Florido. All rights reserved.
//

extension TWClient {
    
    struct Api {
//        https://us-central1-tw-exercicio-mobile.cloudfunctions.net/lookup?id=<id>,<id>,<id>&limit=5
        
        // MARK: URLs
        static let Scheme = "https"
        static let Host = "us-central1-tw-exercicio-mobile.cloudfunctions.net"
    }
    
    struct Methods {
        static let Lookup = "/lookup"
    }
    
    struct ParameterKeys {
        static let Id = "id"
        static let Limit = "limit"
    }
    
    struct JSONResponseKeys {
        
        static let Results = "results"
        static let WrapperType = "wrapperType"
        
        // MARK: Song
        static let SongId = "id"
        static let TrackName = "trackName"
        static let ArtworkUrl = "artworkUrl"
        static let SongArtistId = "artistId"
        static let SongArtistName = "artistName"
        static let SongPrimaryGenreName = "primaryGenreName"
        
        
        //MARK: Artist
        static let ArtistId = "id"
        static let ArtistName = "artistName"
        static let ArtistPrimaryGenreName = "primaryGenreName"
  
    }
    
}

