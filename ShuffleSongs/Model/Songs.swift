//
//  Songs.swift
//  ShuffleSongs
//
//  Created by Silvia Florido on 05/02/19.
//  Copyright © 2019 Silvia Florido. All rights reserved.
//

import Foundation

class Songs {


    var artists: [Artist]?
    lazy var allSongs: [Song]?  = {
        var allSongsFromArtists = [Song]()
        artists?.forEach { (artist) in
            allSongsFromArtists.append(contentsOf: artist.songs)
        }
        return allSongsFromArtists
    }()
    var artistToSkip: [Artist]?
    var songsToDisplay: [Song]?
    
    
    func loadInitialData(_ completionHandler: @escaping (_ error: NSError?) -> Void) {
        TWClient.sharedInstance().getArtistsWithSongs { (artists, error) in
            if error != nil  {
                completionHandler(error)
            } else {
                self.artists = artists
                self.songsToDisplay = self.allSongs
                completionHandler(nil)
                
                
            }
        }
    }
    
    
    func shuffleList() {
        guard let artists = artists, let allSongs = allSongs, allSongs.count > 0 else {
            return
        }
        
        songsToDisplay?.removeAll()

        var artistsMutable = artists.flatMap{ $0.copy() } as! [Artist]
        var selectedArtistIndex  = 0
        var selectedArtist = artistsMutable[selectedArtistIndex]
        var songIndex = 0

        for _ in 0...allSongs.count - 1 {
            let eligibleArtists = artistsMutable.filter{ $0.artistId != selectedArtist.artistId}
            
            guard eligibleArtists.count > 0 else {
                insertRemainingSongs(selectedArtist.songs)
                return
            }
            
            selectedArtistIndex = Int(arc4random_uniform(UInt32(eligibleArtists.count)))
            selectedArtist = eligibleArtists[selectedArtistIndex]

            songIndex = Int(arc4random_uniform(UInt32(selectedArtist.songs.count)))
            let selectedSong = selectedArtist.songs[songIndex]
            songsToDisplay?.append(selectedSong)
            selectedArtist.songs.remove(at: songIndex)
            if let artistIndexToRemove = artistsMutable.index(of: selectedArtist), selectedArtist.songs.count == 0 {
                artistsMutable.remove(at: artistIndexToRemove)
            }

        }
    }
    
    
    private func insertRemainingSongs(_ songs: [Song]) {
        var songs = songs
        while let songToInsert = songs.first, var songsToDisplay = songsToDisplay {
                for index in 0...songsToDisplay.count-1 {
                    let previousSong = songsToDisplay[index]
                    let nextSong = songsToDisplay[index + 1]
                    if previousSong.artistId != songToInsert.artistId  &&  nextSong.artistId != songToInsert.artistId {
                        songsToDisplay.insert(songToInsert, at: index + 1)
                        songs.removeFirst()
                        break
                    }
                }
        }
        
    }
    
}























