//
//  LauchViewController.swift
//  ShuffleSongs
//
//  Created by Silvia Florido on 12/02/19.
//  Copyright © 2019 Silvia Florido. All rights reserved.
//

import UIKit
import Lottie

class LauchViewController: UIViewController {
    var timer: Timer?
    let songs: Songs = Songs()
    let animationLottie: LOTAnimationView =  {
        let animationView = LOTAnimationView(name: "animation-w100-h100")
        animationView.animationSpeed = 0.5
        return animationView
        
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        animationLottie.frame = CGRect(x: 0, y: 0, width: 200, height: 200)
        animationLottie.center = self.view.center
        animationLottie.contentMode = .scaleAspectFill
        view.addSubview(animationLottie)
        
        animationLottie.play()
        animationLottie.loopAnimation = true
        
        timer = Timer.scheduledTimer(timeInterval: 5.0, target: self, selector: #selector(timeOut), userInfo: nil, repeats: false)
        
        loadSongs()
    }
    
    
    private func loadSongs() {
        songs.loadInitialData { (error) in
            if error != nil  {
                //display error message
            } else {
                if let listSongsVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "listSongs") as? ListSongsTableViewController {
                    listSongsVC.songs = self.songs
                    DispatchQueue.main.async {
                        self.navigationController?.pushViewController(listSongsVC, animated: true)
                    }
                }
            }
            self.timer?.invalidate()

        }
    }
    
    
    @objc func timeOut() {
        timer?.invalidate()
        animationLottie.stop()
//         show time out error message
    }
    
    
    
}
