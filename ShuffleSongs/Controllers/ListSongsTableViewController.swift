//
//  ListSongsTableViewController.swift
//  ShuffleSongs
//
//  Created by Silvia Florido on 02/02/19.
//  Copyright © 2019 Silvia Florido. All rights reserved.
//

import UIKit

class ListSongsTableViewController: UITableViewController {
    
    @IBOutlet weak var shufffleButton: UIBarButtonItem!
    
    
    var songs: Songs?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.setHidesBackButton(true, animated:true);
        self.view.isHidden = true
        self.shufffleButton.isEnabled = false
        self.tableView.reloadData()
        self.view.isHidden = false
        self.shufffleButton.isEnabled = true
    }
    
    
    @IBAction func shuffle(_ sender: UIBarButtonItem) {
        songs?.shuffleList()
        self.tableView.reloadData()
    }
    
    

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return songs?.songsToDisplay?.count ?? 0
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "songCell", for: indexPath) as! SongTableViewCell
        
        if let song = songs?.songsToDisplay?[indexPath.row] {
            cell.trackName.text = song.trackName
            if let artistName = song.artistName, let genre = song.primaryGenreName {
                cell.artistNameGenre.text = artistName + " " + genre
            } else {
                cell.artistNameGenre.text = "Unknown"
            }
            
        }

        return cell
    }
  

  
    override func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if let artworkUrl = songs?.songsToDisplay?[indexPath.row].artworkUrl {
            TWClient.sharedInstance().getImageFor(artworkUrl) { (image, error) in
                if error != nil  {
                    //display error message
                } else {
                    if let image = image {
                        DispatchQueue.main.async {
                            (cell as? SongTableViewCell)?.trackImageView.image = image
                        }
                    }
                }
            }            
        }
    }
    
    

}
