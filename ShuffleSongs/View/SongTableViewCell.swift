//
//  SongTableViewCell.swift
//  ShuffleSongs
//
//  Created by Silvia Florido on 02/02/19.
//  Copyright © 2019 Silvia Florido. All rights reserved.
//

import UIKit

class SongTableViewCell: UITableViewCell {

    
    @IBOutlet weak var trackName: UILabel!
    @IBOutlet weak var artistNameGenre: UILabel!
    @IBOutlet weak var trackImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        cleanFields()
    }

    override func prepareForReuse() {
        super.prepareForReuse()
       cleanFields()
    }
    
    private func cleanFields() {
        trackImageView.image = nil
        trackName.text = ""
        artistNameGenre.text = ""
    }

}
